require('dotenv').config()
const {Pool} = require('pg')

const config = {
    user: process.env.DB_USER,
    password: process.env.PASSWORD,
    database: process.env.DATABASE,
    host: process.env.HOST,
    port: parseInt(process.env.DB_PORT)
}

if(process.env.NODE_ENV == "test"){
    config.database = process.env.TEST_DATABASE
}

const pool = new Pool(config)

pool.on('error', (err, client) => {
    console.log(err)
})

module.exports = pool