import db from './db'

export const createUser = async (user) => {
    const newUserQuery = 'INSERT INTO dbo.user(id, email, name, password, created_at) VALUES($1, $2, $3, $4, $5)'
    await db.query(newUserQuery, [user.id, user.email, user.name, user.password, user.createdAt])
}

export const getUserByEmail = async (email) => {
    const getUserQuery = 'SELECT * FROM dbo.user WHERE email=$1'
    const rset =  await db.query(getUserQuery, [email])
    if (rset.rows.length === 0) {
        return null
    }
    return rset.rows[0]
}

export const getUserById = async (id) => {
    const getUserQuery = 'SELECT * FROM dbo.user WHERE id=$1'
    const rset =  await db.query(getUserQuery, [id])
    if (rset.rows.length === 0) {
        return null
    }
    return rset.rows[0]
}

