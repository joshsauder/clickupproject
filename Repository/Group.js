import db from './db'

export const createGroup = async (group) => {
    const newGroupQuery = 'INSERT INTO dbo.group(id, name, admin, created_at) VALUES($1, $2, $3, $4)'
    await db.query(newGroupQuery, [group.id, group.name, group.admin, group.createdAt])
}

export const addUsersToGroup = async (newUser) => {
    const addUserQuery = 'INSERT INTO dbo.user_groups(user_email, group_id) VALUES($1, $2)'
    await db.query(addUserQuery, [newUser.userEmail, newUser.groupId])
}

export const getGroupById = async (userId) => {
    const getGroupQuery = 'SELECT ug.user_email, u.name as "uName", g.id, g.name as "gName" FROM dbo.user_groups as ug'
                        + ' INNER JOIN dbo.group as g ON ug.group_id = g.id'
                        + ' LEFT OUTER JOIN dbo.user as u ON ug.user_email = u.email'
                        + ' WHERE g.admin = $1'
                        + ' ORDER BY g.id'

    const groupData = await db.query(getGroupQuery, [userId])
    return groupData.rows
}