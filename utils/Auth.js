const jwt = require('jsonwebtoken')
const secret = process.env.JWT_SECRET
const aud = process.env.AUDIENCE
const iss = process.env.ISSUER

export const generateToken = (email) => {
    const payload = {email: email}
    const options = {audience: aud, issuer: iss, expiresIn: '1hr', }
    
    const token = jwt.sign(payload, secret, options);
    return token
}

export const checkToken = (req, res, next) => {
    const authHeader = req.headers.authorization
    if(authHeader){
        const token = req.headers.authorization.split(' ')[1]
        try {
            const decoded = jwt.verify(token, secret, {audience: aud, issuer: iss})
            if(!decoded || decoded.email == undefined){
                throw new Error("Invalid Token... Unauthorized.")
            }
            req.email = decoded.email
            next()
        }catch(e){
            res.status(401).send()
        }
    } else {
        res.status(401).send()
    }
}