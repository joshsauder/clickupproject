var express = require('express')
var app = express()

const bodyParser = require('body-parser')
import routes from './Controller'

var port = process.env.PORT || 8080;

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

app.use('/api', routes())

app.use((err, req, res, next) => {
    res.status(err.status || 500).send({
        error:{
            status: err.status || 500,
            message: err.message || "Internal Service Error"
        }
    })
})

app.listen(port, () => {
    console.log(`Server is listening on port ${port}`)
})

module.exports = app;