const express = require('express')
const router = express.Router()

const {createNewGroup, addUser, getGroup} = require('../Service/Group')
const {checkToken} = require('../utils/Auth')

export default (app) => {
    app.use('/group', router)

    /**
     * Gets a users groups by id. Route requires auth token
     *
     * @param id - User ID
     * @returns Group object 
     */
    router.get('/:id', checkToken, async (req, res, next) => {
        const { id } = req.params
        try{
            const group = await getGroup(id)
            res.json(group)
        }catch(e){
            next(e)
        }

    })

    /**
     * Posts a user by email to a group. Route requires auth token
     *
     * @param body - User email and group ID (email: String, groupId: UUID)
     */
    router.post('/user', checkToken, async (req, res, next) => {
        const { email, groupId } = req.body
        try {
            await addUser(email, groupId)
            res.status(200).send()
        }catch(e){
            next(e)
        }
    })

    /**
     * Posts a new group. Route requires auth token
     *
     * @param body - new group object (name: String, admin: UUID)
     * @returns Group object
     */
    router.post('', checkToken, async (req, res, next) => {
        const newGroup = req.body
        try {
            const group = await createNewGroup(newGroup)
            res.json(group)
        }catch(e){
            next(e)
        }
    })
}