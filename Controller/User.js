const express = require('express')
const router = express.Router()
const {createNewUser, getUser, getUserViaJWT} = require('../Service/User')
const {checkToken} = require('../utils/Auth')


export default (app) => {
    app.use('/user', router)

    /**
     * Authenticates a user.
     *
     * @param body - User email and password (email: String, password: String)
     * @returns User object and auth token.
     */
    router.post('/auth', async (req, res, next) => {
        const { email, password } = req.body
        try {
            const user = await getUser(email, password)
            res.json(user)
        } catch(e){
            next(e)
        }
    })

    /**
     * Authenticates a user via JWT
     * 
     * @returns User object
     */
    router.get("/auth", checkToken,  async (req, res, next) => {
        const email = req.email
        try {
            const user = await getUserViaJWT(email)
            res.json(user)
        } catch(e){
            next(e)
        }
    })

    /**
     * Posts a new user.
     *
     * @param user - User object (email: String, name: String, password: String)
     * @returns User object (without password) and auth token.
     */
    router.post('', async (req, res, next) => {
        const user = req.body
        try{
            const newUser = await createNewUser(user)
            res.json(newUser)
        }catch(e){
            next(e)
        }
    })
}
