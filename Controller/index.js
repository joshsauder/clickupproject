const { Router } = require('express');
import GroupController from './Group'
import UserController from './User'

export default () => {
    const router = Router()
    GroupController(router)
    UserController(router)

    return router
}