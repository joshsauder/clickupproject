const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
chai.should()

const app = require('../index');
const db = require('../Repository/db')

describe('User', () => {

    before((done) => {
        db.query(
            "CREATE SCHEMA dbo; \n" + 
            "CREATE TABLE dbo.USER (ID UUID PRIMARY KEY, Email VARCHAR(255) NOT NULL UNIQUE, Name VARCHAR(255) NOT NULL, Password CHAR(64) NOT NULL, Created_At TIMESTAMP NOT NULL); \n" +
            "CREATE TABLE dbo.GROUP (ID UUID PRIMARY KEY, Name VARCHAR(255) NOT NULL, Admin UUID NOT NULL, Created_At TIMESTAMP NOT NULL, FOREIGN KEY (Admin) REFERENCES dbo.user (ID)); \n" +
            "CREATE TABLE dbo.USER_GROUPS (user_email VARCHAR(255), Group_id UUID, FOREIGN KEY (Group_id) REFERENCES dbo.group (ID), CONSTRAINT PK_USER_GROUP PRIMARY KEY (user_email, group_id));"
        ).then(res => {
            done()
        })
    })

    after((done) => {
        db.query(
            "DROP TABLE dbo.user_groups; \n" +
            "DROP TABLE dbo.group; \n" +
            "DROP TABLE dbo.user; \n" +
            "DROP SCHEMA dbo"
        ).then(() => {
            done()
        })
    })

    it('Sign up new user', (done) => {
        let postData = {
            "email": "test@gmail",
            "password": "test2",
            "name": "test2"
        }

        chai.request(app)
        .post('/api/user')
        .send(postData)
        .end((err, res) => {
            chai.expect(res.status).to.equal(200)
            chai.expect(res.body).to.haveOwnProperty('token')
            done();
        });
    })

    it('Sign up new user with missing data', (done) => {
        let postData = {
            "email": "test@gmail",
            "name": "test2"
        }

        chai.request(app)
        .post('/api/user')
        .send(postData)
        .end((err, res) => {
            chai.expect(res.status).to.not.equal(200)
            done();
        });
    })

    it('Sign up user who already exists', (done) => {
        let postData = {
            "email": "test@gmail",
            "password": "test2",
            "name": "test2"
        }

        chai.request(app)
        .post('/api/user')
        .send(postData)
        .end((err, res) => {
            chai.expect(res.status).to.not.equal(200)
            done();
        });
    })

    it('Sign in user who exists', (done) => {
        let postData = {
            "email": "test@gmail",
            "password": "test2",
        }

        chai.request(app)
        .post('/api/user/auth')
        .send(postData)
        .end((err, res) => {
            chai.expect(res.status).to.equal(400)
            chai.expect(res.body).to.not.haveOwnProperty('token')
            done();
        });
    })

    it('Sign in user with incorrect password', (done) => {
        let postData = {
            "email": "test@gmail",
            "password": "incorrect",
        }

        chai.request(app)
        .post('/api/user/auth')
        .send(postData)
        .end((err, res) => {
            chai.expect(res.status).to.equal(400)
            chai.expect(res.body).to.not.haveOwnProperty('token')
            done();
        });
    })
})