const chai = require("chai");
const chaiHttp = require("chai-http");
chai.use(chaiHttp);
chai.should();

const app = require("../index");
const db = require("../Repository/db");
const { createNewUser } = require("../Service/User");
const { createNewGroup } = require("../Service/Group");

describe("Group", () => {
  let id = "";
  let token = "";

  before((done) => {
    db.query(
        "CREATE SCHEMA dbo; \n" + 
        "CREATE TABLE dbo.USER (ID UUID PRIMARY KEY, Email VARCHAR(255) NOT NULL UNIQUE, Name VARCHAR(255) NOT NULL, Password CHAR(64) NOT NULL, Created_At TIMESTAMP NOT NULL); \n" +
        "CREATE TABLE dbo.GROUP (ID UUID PRIMARY KEY, Name VARCHAR(255) NOT NULL, Admin UUID NOT NULL, Created_At TIMESTAMP NOT NULL, FOREIGN KEY (Admin) REFERENCES dbo.user (ID)); \n" +
        "CREATE TABLE dbo.USER_GROUPS (user_email VARCHAR(255), Group_id UUID, FOREIGN KEY (Group_id) REFERENCES dbo.group (ID), CONSTRAINT PK_USER_GROUP PRIMARY KEY (user_email, group_id));"
    ).then((vals) => {
        createNewUser({
            email: "test3@gmail.com",
            password: "test",
            name: "josh",
        }).then((res) => {
            id = res.user.id;
            token = res.token;
            done();
        });
    });
  });

  after((done) => {
    db.query(
        "DROP TABLE dbo.user_groups; \n" +
        "DROP TABLE dbo.group; \n" +
        "DROP TABLE dbo.user; \n" +
        "DROP SCHEMA dbo"
    ).then(() =>{
        done();
    })
  });

  describe("Create Group", () => {
    it("Create new group", (done) => {
      let postData = {
        name: "testGroup",
        admin: id,
      };

      chai
        .request(app)
        .post("/api/group")
        .set({ Authorization: `Bearer ${token}` })
        .send(postData)
        .end((err, res) => {
          chai.expect(res.status).to.equal(200);
          chai.expect(res.body).to.haveOwnProperty("id");
          done();
        });
    });

    it("Create new group without name", (done) => {
      let postData = {
        admin: id,
      };

      chai
        .request(app)
        .post("/api/group")
        .set({ Authorization: `Bearer ${token}` })
        .send(postData)
        .end((err, res) => {
          chai.expect(res.status).to.not.equal(200);
          done();
        });
    });

    it("Create new group user not exist", (done) => {
      let postData = {
        name: "testgroup",
        admin: "8b32c566-cabb-4b77-b8e5-1f22153cab9f",
      };

      chai
        .request(app)
        .post("/api/group")
        .set({ Authorization: `Bearer ${token}` })
        .send(postData)
        .end((err, res) => {
          chai.expect(res.status).to.not.equal(200);
          done();
        });
    });

    it("Create new group without auth token", (done) => {
      let postData = {
        name: "testgroup23",
        admin: "8b32c566-cabb-4b77-b8e5-1f22153cab9f",
      };

      chai
        .request(app)
        .post("/api/group")
        .send(postData)
        .end((err, res) => {
          chai.expect(res.status).to.equal(401);
          done();
        });
    });
  });

  describe("Add Users to Group", () => {
    let groupId;
    before((done) => {
      createNewGroup({ name: "test2", admin: id }).then((res) => {
        groupId = res.id;
        done();
      });
    });

    it("Add user exists to group", (done) => {
      let postData = {
        email: "test2@gmail.com",
        groupId: groupId,
      };

      chai
        .request(app)
        .post("/api/group/user")
        .set({ Authorization: `Bearer ${token}` })
        .send(postData)
        .end((err, res) => {
          chai.expect(res.status).to.equal(200);
          done();
        });
    });

    it("Add user not exists to group", (done) => {
      let postData = {
        email: "test@yahoo.com",
        groupId: groupId,
      };

      chai
        .request(app)
        .post("/api/group/user")
        .set({ Authorization: `Bearer ${token}` })
        .send(postData)
        .end((err, res) => {
          chai.expect(res.status).to.equal(200);
          done();
        });
    });

    it("Add user to group without auth token", (done) => {
      let postData = {
        email: "test@me.com",
        groupId: groupId,
      };

      chai
        .request(app)
        .post("/api/group/user")
        .send(postData)
        .end((err, res) => {
          chai.expect(res.status).to.equal(401);
          done();
        });
    });
  });

  describe("Get Admin Group Data", () => {
    it("Get Group data", (done) => {
      chai
        .request(app)
        .get("/api/group/" + id)
        .set({ Authorization: `Bearer ${token}` })
        .end((err, res) => {
          chai.expect(res.status).to.equal(200);
          done();
        });
    });

    it("Get Group data without group", (done) => {
      chai
        .request(app)
        .get("/api/group/" + "8b32c566-cabb-4b77-b8e5-1f22153cab9f")
        .set({ Authorization: `Bearer ${token}` })
        .end((err, res) => {
          chai.expect(res.status).to.equal(200);
          done();
        });
    });

    it("Get Group data without auth token", (done) => {
      chai
        .request(app)
        .get("/api/group/" + "8b32c566-cabb-4b77-b8e5-1f22153cab9f")
        .end((err, res) => {
          chai.expect(res.status).to.equal(401);
          done();
        });
    });
  });
});
