CREATE TABLE dbo.USER_GROUPS (
    user_email VARCHAR(255) NOT NULL,
    Group_id UUID NOT NULL,
    FOREIGN KEY (Group_id) REFERENCES dbo.group (ID),
    CONSTRAINT PK_USER_GROUP PRIMARY KEY (user_email, group_id)
)