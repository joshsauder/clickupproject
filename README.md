# ClickUp Project
### Josh Sauder

## Routes
1. /api/user - POST
    - Creates a new user and passes back a JWT token.
    - Request body will contain the following: email: String, name: String, password: String
2. /api/user/auth - POST
    - Authenticates a user and passes back a JWT token.
    - Request body will contain the following: email: String, password: String
3. /api/user/auth - GET
    - Authenticates a user by JWT.
    - Returns user info if JWT is valid
    - Route will require the Authorization header to be set to "Bearer <JWT Token>"
4. /api/group - POST
    - Creates a new group.
    - Request body will contain the following: name: String, admin (user ID): UUID
    - Route will require the Authorization header to be set to "Bearer <JWT Token>"
5. /api/group/user - POST
    - Adds a user by email to a group.
    - Request body will contain the following: email: String, groupId: UUID
    - Route will require the Authorization header to be set to "Bearer <JWT Token>"
6. /api/group/:id - GET
    - Gets the groups the user is an admin of.
    - Request param ID will be the users ID.
    - Route will require the Authorization header to be set to "Bearer <JWT Token>"

## Setup
### Start
- Run ```npm install```
- Start a Postgres Docker container. I listed the command below:
```
docker run -it -e "POSTGRES_PASSWORD=miniProj" -e "POSTGRES_USER=test" -e "POSTGRES_DB=dbo"  -p 5412:5432 postgres:latest
```
- Run the SQL scripts located in the SQL directory. Run them in the following order:  
    1. CreateSchema.sql
    2. User.sql
    3. Group.sql
    4. User_Groups.sql

- You will then need to run ```npm start```; the application should be accessible on port 8080.

### Testing

- Start a separate Postgres Docker container. I listed the command below:
```
docker run -it -e "POSTGRES_PASSWORD=miniProj" -e "POSTGRES_USER=test" -e "POSTGRES_DB=test"  -p 5412:5432 postgres:latest
```
- You will then need to run ```npm run test``` in order to run the tests. The tables should be created before each set of tests, and dropped after each set of tests.

## Items that would be needed for a production deployment
- Password and email validation. A password should at least contain a uppercase and lowercase letter, a number, and be at least 8 characters in length.
- Removing the .env file from the git repo. This file should only be passed to users who are authorized to make changes to this application.
- A way for the user to update his/her user information, and group information.

## Time Estimation
- 4-5 hours on building the application
- 2 hours on testing
- 6-7 hours total