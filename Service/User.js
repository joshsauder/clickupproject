import { generateToken } from '../utils/Auth';

const {createUser, getUserByEmail} = require('../Repository/User')
const ErrorHandler = require('../utils/ErrorHandler')
const User = require('../Model/User')
const uuidv4 = require('uuid/v4');
const { genSalt, hash, compare } = require('bcrypt');

const SALT_FACTOR = 10


export const createNewUser = async (user) => {
    let date = new Date().toISOString();
    const hash = await hashPassword(user.password)

    const postData = createUserObj(user, uuidv4(), date, hash)
    postData.validateInputs()

    await createUser(postData)

    const token = generateToken(postData.email)
    return createResponseObj(postData, token);
}

export const getUser = async (email, password) => {
    let userObj = await getUserObj(email)
    const isMatch = await checkPass(password, userObj.password)
    
    if(!isMatch){
        throw new ErrorHandler(400, "Invalid Password")
    }

    const token = generateToken(userObj.email)
    return createResponseObj(userObj, token);
}

export const getUserViaJWT = async (email) => {
    const userObj = await getUserObj(email)
    return createResponseObj(userObj, undefined);
}

const getUserObj = async (email) => {
    const user = await getUserByEmail(email)
    if(user == null){ 
        throw new ErrorHandler(404, "User does not exist")
    }
    return createUserObj(user, user.id, user.created_at, user.password)
}

const hashPassword = async (password) => {
    if(password == undefined || password.length == 0){
        throw new ErrorHandler(400, "Password is Required")
    }
    const salt = await genSalt(SALT_FACTOR)
    const hashedPass = await hash(password, salt)
    return hashedPass
}

const checkPass = async (candidatePassword, hashedPassword) => {
    const isMatch = await compare(candidatePassword, hashedPassword)
    return isMatch
}

const createUserObj =  (user, id, date, hash) => {
    let userObj = new User(id, user.email, user.name, hash, date)
    return userObj
}

const createResponseObj = (user, token) => {
    return {
        user: {
            id: user.id,
            email: user.email,
            name: user.name,
            createdAt: user.createdAt
        },
        token: token
    }
}