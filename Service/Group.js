const {createGroup, addUsersToGroup, getGroupById} = require('../Repository/Group')
const ErrorHandler = require('../utils/ErrorHandler')
const uuidv4 = require('uuid/v4');
const Group = require('../Model/Group')
const {getUserById} = require('../Repository/User')

export const createNewGroup = async (group) => {
    let date = new Date().toISOString();
    let postData = createGroupObj(group, uuidv4(), date)
    postData.validateInputs()

    let user = await getUserById(group.admin)
    if(user == null){ 
        throw new ErrorHandler(400, "User does not exist")
    }
    let userGroupPostData = createUserGroupObj(user.email, postData.id)

    await createGroup(postData)
    await addUsersToGroup(userGroupPostData)
    return postData
}

export const addUser = async (email, groupId) => {
    if(!validateEmail(email)){
        throw new ErrorHandler(400, "Invalid Email")
    }

    if(groupId === undefined || groupId.length !== 36){
        throw new ErrorHandler(400, "Invalid Group ID")
    }
    
    const userGroup = createUserGroupObj(email, groupId)
    await addUsersToGroup(userGroup)
}

export const getGroup = async (userId) => {
    const groupData = await getGroupById(userId)
    return createGetAllGroupsObj(groupData)
}

const validateEmail = (email) => {
    if(email == undefined || email.length < 3) return false 
    let i = email.indexOf('@')
    return i > 0 && i < email.length - 1
}

const createUserGroupObj = (email, groupId) => {
    return {
        userEmail: email,
        groupId: groupId
    }
}

const createGetAllGroupsObj = (arr) => {
    let retObj = {}
    arr.forEach(obj => {
        if(retObj[obj.id] === undefined){
            retObj[obj.id] = {groupName: obj.gName, users: []}
        }

        retObj[obj.id].users.push({name: obj.uName, email: obj.user_email})
    })

    return retObj
}

const createGroupObj = (group, id, date) => {
    const groupObj = new Group(id, group.name, group.admin, date)
    return groupObj
}
