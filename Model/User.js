import ErrorHandler from '../utils/ErrorHandler'

class User {
    constructor(id, email, name, password, createdAt){
        this.id = id,
        this.email = email
        this.name = name
        this.password = password
        this.createdAt = createdAt
    }
}

User.prototype.validateInputs = function(){
    if(this.email == undefined || this.email.length < 3){
         throw new ErrorHandler(400, "Invalid Email") 
    }

    let i = this.email.indexOf('@')
    if(i < 1 || i == this.email.length - 1){
        throw new ErrorHandler(400, "Invalid Email") 
    }

    if(this.name == undefined || this.name.length == 0){
        throw new ErrorHandler(400, "Invalid Name")
    }
}

module.exports = User