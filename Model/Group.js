import ErrorHandler from '../utils/ErrorHandler'

class Group {
    constructor(id, name, admin, createdAt){
        this.id = id,
        this.name = name
        this.admin = admin
        this.createdAt = createdAt
    }
}

Group.prototype.validateInputs = function(){
    if(this.name == undefined || this.name.length === 0){
        throw new ErrorHandler(400, "Invalid Name")
    }

    //uuid length = 36
    if(this.admin == undefined || this.admin.length !== 36){
        throw new ErrorHandler(400, "Invalid Name")
    }
}

module.exports = Group